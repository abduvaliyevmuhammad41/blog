# Generated by Django 4.1.3 on 2022-11-30 13:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('apps', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='is_active',
            field=models.CharField(choices=[('active', 'active'), ('canceled', 'canceled'), ('pending', 'pending')], default='pending', max_length=8),
        ),
        migrations.AlterField(
            model_name='blog',
            name='category',
            field=models.ManyToManyField(to='apps.category'),
        ),
    ]
